# README #
create an android app similar to [Sensoduino](https://play.google.com/store/apps/developer?id=Hazim%20Bitar&hl=en)

* reference: [techbitar](http://www.techbitar.com/)

# To do #
## v0.1 - display arduino readings on android
* v0.1.1 dispaly sensor reading by button click (**Done** 1/10/17)
* v0.1.2 monitor sensor reading continously by thread (** Done ** 1/11/17)
* **HIGH** add a refresh button  
## v0.2 - improved android GUI widgets and user experience  
## v0.3 - sensor harvest - list and display android sensor readings such as agps location, gyro etc. 

# Help #
* how to [setup](http://stackoverflow.com/questions/19099244/how-to-import-a-project-into-bitbucket-repository-from-android-studio) bicketbucket with Android Studio