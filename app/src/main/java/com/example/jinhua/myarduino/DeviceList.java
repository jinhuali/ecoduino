package com.example.jinhua.myarduino;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.graphics.drawable.DrawerArrowDrawable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Set;

public class DeviceList extends AppCompatActivity {
    //widgets
    Button btnPaired;
    ListView lstDevice;
    //Bluetooth
    private BluetoothAdapter myBluetooth = null;
    private Set<BluetoothDevice> pairedDevices;
    //private OutputStream outstream = null;
    public static String EXTRA_ADDRESS = "device_address";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_list);

        //Calling widgets
        btnPaired = (Button)findViewById(R.id.button);
        lstDevice = (ListView)findViewById(R.id.listview);

        checkBTStatus();

        btnPaired.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                pairedDevicesList();
            }
        });
    }

    private void checkBTStatus(){
        myBluetooth = BluetoothAdapter.getDefaultAdapter();
        if(myBluetooth == null){
            Toast.makeText(getApplicationContext(), "Bluetooth Device Not Available", Toast.LENGTH_LONG).show();
            finish(); //finish apk
        }else if(!myBluetooth.isEnabled()){
            //Ask user to turn bluetooth on
            Intent turnBTon = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(turnBTon, 1);
        }
    }

    private void pairedDevicesList(){
        pairedDevices = myBluetooth.getBondedDevices();
        ArrayList list = new ArrayList();
        if(pairedDevices.size()>0){
            for(BluetoothDevice bt:pairedDevices){
                list.add(bt.getName()+'\n'+bt.getAddress());
            }
        }else{
            Toast.makeText(getApplicationContext(), "No Paired Bluetooth Devices Found", Toast.LENGTH_LONG).show();
        }
        final ArrayAdapter<String> adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, list);
        lstDevice.setAdapter(adapter);
        lstDevice.setOnItemClickListener(myListClickListerner); //method called when the device from the list is clicked
    }

    private AdapterView.OnItemClickListener myListClickListerner = new AdapterView.OnItemClickListener(){
        public void onItemClick(AdapterView<?> av, View v, int arg2, long arg3){
            //get device MAC address, the last 17 chars in the view
            String info = ((TextView) v).getText().toString();
            String address = info.substring(info.length()-17);

            //make an intent to start next activity
            //Intent i = new Intent(DeviceList.this, ledControl.class);
            Intent i = new Intent(DeviceList.this, pmReader.class);
            i.putExtra(EXTRA_ADDRESS, address); // this willl be received at ledControl (class) activity
            startActivity(i);
        }
    };
}
