package com.example.jinhua.myarduino;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.Toast;

import java.io.IOException;
import java.util.UUID;

public class ledControl extends AppCompatActivity {
    Button btnOn, btnOff, btnDis;
    SeekBar brightness;
    String address = null;
    private ProgressDialog progress;
    BluetoothAdapter myBluetooth = null;
    BluetoothSocket btSocket = null;
    private boolean isBtConnected = false;
    static final UUID myUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_led_control);

        Intent newint = getIntent();
        address = newint.getStringExtra(DeviceList.EXTRA_ADDRESS); //receive MAC address of the bluetooth device

        //call the widgets
        btnOn = (Button)findViewById(R.id.button2);
        btnOff = (Button)findViewById(R.id.button3);
        btnDis = (Button)findViewById(R.id.button4);
        brightness = (SeekBar)findViewById(R.id.seekBar);

        new ConnectBT().execute(); //call the class to execute

        btnOn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                turnOnLed(); //turn on led
            }
        });

        btnOff.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                turnOffLed(); //turn off led
            }
        });

        btnDis.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Disconnect(); //close connection
            }
        });
    }


    private void Disconnect(){
        if(btSocket != null){
            try {
                btSocket.close(); //close connection
            }catch(IOException e){
                msg("Error");
            }
            finish(); //return to first layout
        }
    }

    private void turnOffLed(){
        if(btSocket != null){
            try{
                btSocket.getOutputStream().write("LF".toString().getBytes());
            }catch(IOException e){
                msg("Error");
            }
        }
    }

    private void turnOnLed(){
        if(btSocket != null){
            try{
                btSocket.getOutputStream().write("LO".toString().getBytes());
            }catch(IOException e){
                msg("Error");
            }
        }
    }

    private void msg(String s){
        Toast.makeText(getApplicationContext(), s, Toast.LENGTH_LONG).show();
    }

    private class ConnectBT extends AsyncTask<Void, Void, Void> // UI thread
    {
        private boolean ConnectSuccess = true;
        @Override
        protected void onPreExecute(){
            //show a progress dialog
            progress = ProgressDialog.show(ledControl.this, "Connecting...", "Pleasee wait!!!");
        }

        @Override
        protected Void doInBackground(Void... devices){
            try{
                if(btSocket == null || !isBtConnected) {
                    myBluetooth = BluetoothAdapter.getDefaultAdapter();
                    BluetoothDevice dispositivo = myBluetooth.getRemoteDevice(address);
                    btSocket = dispositivo.createInsecureRfcommSocketToServiceRecord(myUUID);
                    BluetoothAdapter.getDefaultAdapter().cancelDiscovery();
                    btSocket.connect(); //start connection
                }
            }catch (IOException e){
                ConnectSuccess = false;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) //after doInBackground, checks whether everything is fine
        {
            super.onPostExecute(result);
            if(!ConnectSuccess){
                msg("Connection Failed. Is it a SPP Bluetooth? Try again.");
                finish();
            }else{
                msg("Connected.");
                isBtConnected = true;
            }
            progress.dismiss();
        }
    }
}
